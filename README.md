https://flask.palletsprojects.com/en/1.1.x/

## Stats
https://pypistats.org/packages/flask

## Install
$ pip install Flask

## Run
$ export FLASK_APP=hello.py
$ flask run